It's test task for Tallink.

Before application start please run 'npm install'.

To run the project please execute 'npm start'.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

To run tests please execute 'npm test'.