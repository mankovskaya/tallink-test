import React from 'react';
import './App.css';
import OperationsHistory from "./layouts/operations-history/Operations-history";
import Context from "./context";
import Calculator from "./layouts/calculator/Calculator";
import Sum from "./layouts/calculator/operations-buttons/operations/Sum";
import Divide from "./layouts/calculator/operations-buttons/operations/Divide";
import RemainderOfDivision from "./layouts/calculator/operations-buttons/operations/RemainderOfDivision";
import MaxPrimeNumber from "./layouts/calculator/operations-buttons/operations/MaxPrimeNumber";

const App = () => {
    const [resultsHistory, setHistory]  = React.useState([]);
    const [firstField, setFirstField] = React.useState('');
    const [secondField, setSecondField] = React.useState('');

    let updateHistory = (history, firstField, secondField, operationColor, result) => {
        setHistory([
            ...history,
            {
                firstNumber: firstField,
                secondNumber: secondField,
                operationColor: operationColor,
                result: result
            }
        ]);
    }

    const availableOperations = [
        new Sum(),
        new Divide(),
        new RemainderOfDivision(),
        new MaxPrimeNumber()
    ];

    return (
        <Context.Provider value={{
            resultsHistory,
            availableOperations,
            setFirstField,
            setSecondField,
            updateHistory
        }}>
            <div className="App">
                <Calculator firstField ={firstField} secondField={secondField} />
                <OperationsHistory resultsHistory={resultsHistory}/>
            </div>
        </Context.Provider>
    );
}

export default App;
