import { render } from '@testing-library/react';
import App from './App';
import React from "react";

describe("App component", ()=> {

    it("should render correctly", () => {

        let app = render(<App/>);

        expect(app).toBeTruthy();
    });
})