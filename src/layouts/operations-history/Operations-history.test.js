import { render } from '@testing-library/react';
import OperationsHistory from "./Operations-history";
import React from "react";


describe("Render OperationsHistory component", ()=> {

    describe("without history", ()=>{
        it("should not render operations", () => {
            let history = [];

            const { queryAllByTestId } = render(<OperationsHistory resultsHistory={history} />)

            expect(queryAllByTestId("operation").length).toBe(0);
        });

        it("should render message about empty history", () => {
            let history = [];

            const { queryAllByTestId } = render(<OperationsHistory resultsHistory={history} />)

            expect(queryAllByTestId("empty-history")).toBeTruthy();
        });
    })

    describe("with history", ()=>{
        it("should render expected elements count", () => {
            let history = [
                {
                    firstNumber: 5,
                    secondNumber: 10,
                    result: 55
                },
                {
                    firstNumber: 3,
                    secondNumber: 10,
                    result: 13
                }
            ]
            const { queryAllByTestId } = render(<OperationsHistory resultsHistory={history} />)

            expect(queryAllByTestId("operation").length).toBe(2);
        });
    })

    describe("with one item", ()=>{
        it("should render text correctly", () => {
            let history = [
                {
                    firstNumber: 5,
                    secondNumber: 10,
                    operationText: 'Test1',
                    result: 55
                }
            ]
            const { queryAllByTestId } = render(<OperationsHistory resultsHistory={history} />)

            expect(queryAllByTestId("operation")[0].textContent).toBe("Input data: (5, 10), result: 55");
        });

        it("should apply color to element", () => {
            let history = [
                {
                    firstNumber: 5,
                    secondNumber: 10,
                    operationColor: '#F59E8B',
                    result: 55
                }
            ]
            const { queryByTestId } = render(<OperationsHistory resultsHistory={history} />)
            const style = window.getComputedStyle(queryByTestId("operation"));

            expect(rgb2hex(style.backgroundColor)).toBe('#F59E8B');
        });
    })


})

function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return ("#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3])).toUpperCase();
}