import React from 'react';
import classes from './Operations-history.module.scss';

const OperationsHistory = (props) => {
    let operations = props.resultsHistory.map((item, index) => {
        return <tr key={index} data-testid="operation" style={{backgroundColor: item.operationColor}}>
            <td className={classes.historyTableRow}>{`Input data: (${item.firstNumber}, ${item.secondNumber}), result: ${item.result}`}</td>
        </tr>
    })
    return (
        <div data-testid="history"
             className={classes.wrapper}>
            <table className={classes.historyTableWrapper}>
                <thead>
                <tr>
                    <th className={classes.historyTableName}>
                        <p>History</p>
                    </th>
                </tr>
                </thead>
                <tbody>
                {operations.length
                    ? operations
                    : <tr className={classes.textEmptyHistory}>
                        <td data-testid="empty-history">History is empty</td>
                    </tr>}
                </tbody>
            </table>
        </div>
    );
}

export default OperationsHistory;
