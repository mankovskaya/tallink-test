import {fireEvent, render} from '@testing-library/react';
import React from "react";
import Context from '../../context';
import Calculator from "./Calculator";

describe("Calculator component", ()=> {

    it("should render correctly", () => {

        const setFirstField = jest.fn();
        const setSecondField = jest.fn();
        const availableOperations = [];
        const context = { setFirstField, setSecondField, availableOperations } ;

        const { queryAllByTestId } = renderCalculator(context);
        expect(queryAllByTestId("fields-container")).toBeTruthy();
    });

    it ("first field value update on change", () => {
        const setFirstField = jest.fn();
        const setSecondField = jest.fn();
        const availableOperations = [];
        const context = { setFirstField, setSecondField, availableOperations} ;
        const { queryByTestId } = renderCalculator(context);
        const firstField = queryByTestId("first-field");

        fireEvent.change(firstField, {target: {value: 234}});

        expect(setFirstField).toHaveBeenCalledWith(234);
    })

    it ("second field value update on change", () => {
        const setFirstField = jest.fn();
        const setSecondField = jest.fn();
        const availableOperations = [];
        const context = { setFirstField, setSecondField, availableOperations} ;
        const { queryByTestId } = renderCalculator(context);
        const secondField = queryByTestId("second-field");

        fireEvent.change(secondField, {target: {value: 65656}});

        expect(setSecondField).toHaveBeenCalledWith(65656);
    })
})


const renderCalculator = (context, firstValue , secondValue) => {
    return render(
        <Context.Provider value={ context } >
            <Calculator firstField={firstValue} secondField={secondValue} />
        </Context.Provider>)
}