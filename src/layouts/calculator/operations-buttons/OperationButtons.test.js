import { render, fireEvent } from '@testing-library/react';
import React from "react";
import OperationsButtons from "./OperationsButtons";
import Context from "../../../context";


describe("OperationButtons component", ()=> {

    it("should render correctly", () => {
        let availableOperations = []
        let resultsHistory = [];
        let context = { availableOperations, resultsHistory};

        const { queryAllByTestId } = renderOperationButtons(context)

        expect(queryAllByTestId("buttons-container")).toBeTruthy();
    });


    it("should render buttons with proper backgroundColor", () => {
        let availableOperations = [
            {
                action : jest.fn().mockReturnValue(5),
                getValidationErrors: jest.fn(),
                operationColor: '#F59E8B'
            }
        ]
        let resultsHistory = [];
        let updateHistory = jest.fn();
        let context = { availableOperations, resultsHistory, updateHistory};

        const { queryByTestId } = renderOperationButtons(context, 1,2);
        const style = window.getComputedStyle(queryByTestId("operation-button"));

        expect(rgb2hex(style.backgroundColor)).toBe('#F59E8B');
    });

    it("should call 'getValidationErrors' with proper arguments", () => {

        let availableOperations = [
            {
                action : jest.fn().mockReturnValue(5),
                getValidationErrors: jest.fn()
            }
        ]
        let context = { availableOperations, resultsHistory: [], updateHistory: jest.fn()};

        const { queryByTestId } = renderOperationButtons(context, 1,2)
        fireEvent.click(queryByTestId("operation-button"))

        expect(availableOperations[0].getValidationErrors).toHaveBeenCalledWith(1, 2);
    });

    it("should call 'alert' when errors exist", () => {

        let availableOperations = [
            {
                action : jest.fn(),
                getValidationErrors: jest.fn().mockReturnValue("superError")
            }
        ]
        let context = { availableOperations, resultsHistory: [], updateHistory: jest.fn()};
        global.alert = jest.fn();

        const { queryByTestId } = renderOperationButtons(context, 1,2)
        fireEvent.click(queryByTestId("operation-button"))

        expect(global.alert).toHaveBeenCalledWith("superError")
    });

    it("should not call 'updateHistory' when errors exist", () => {

        let availableOperations = [
            {
                action : jest.fn(),
                getValidationErrors: jest.fn().mockReturnValue("superError")
            }
        ]
        global.alert = jest.fn();
        let updateHistory = jest.fn();
        let context = { availableOperations, resultsHistory: [], updateHistory};

        const { queryByTestId } = renderOperationButtons(context, 1,2)
        fireEvent.click(queryByTestId("operation-button"))

        expect(updateHistory).not.toHaveBeenCalled()
    });

    it("should call 'updateHistory' when proper arguments", () => {

        let firstField = 5;
        let secondField = 6;
        let resultsHistory = [];
        let operationColor = '#F59E8B';

        let availableOperations = [
            {
                action : jest.fn(firstField, secondField).mockReturnValue(11),
                getValidationErrors: jest.fn(),
                operationColor: operationColor
            }
        ]
        global.alert = jest.fn();
        let updateHistory = jest.fn();
        let context = { availableOperations, resultsHistory, updateHistory};

        const { queryByTestId } = renderOperationButtons(context, firstField,secondField)
        fireEvent.click(queryByTestId("operation-button"))

        expect(updateHistory).toHaveBeenCalledWith(
            resultsHistory,
            firstField,
            secondField,
            operationColor,
            "11.00"
        )
    });

})


const renderOperationButtons = (context, firstValue , secondValue) => {
    return render(
        <Context.Provider value={ context } >
            <OperationsButtons firstField={firstValue} secondField={secondValue} />
        </Context.Provider>)
}

function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return ("#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3])).toUpperCase();
}