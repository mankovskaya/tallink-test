export default class MaxPrimeNumber {
    get text(){
        return 'Max prime number from range';
    }

    get operationColor(){
        return '#7ACB99';
    }

    action(fromValue, toValue){
        let maxPrimeNumber;
        let from = Math.round(fromValue);
        let to = Math.round(toValue);

        for (let i = from; i <= to; i++) {

            let isPrime = true;
            for (let j = 2; j < i; j++) {
                if (i % j === 0){
                    isPrime = false;
                    break;
                }
            }

            if (isPrime){
                maxPrimeNumber = i;
            }
        }

        return maxPrimeNumber;
    }

    getValidationErrors(a, b){
        let result = "";
        if(isNaN(a)){
            result +="First value should be defined\r\n";
        }

        if(isNaN(b)){
            result +="Second value should be defined\r\n";
        }

        if(a > b){
            return "Second value should be greater than first value";
        }

        if(a <2 &&  b < 2){
            return "Minimum prime number is 2";
        }

        return result;
    }
}