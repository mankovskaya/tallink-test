export default class RemainderOfDivision {
    get text(){
        return 'Remainder of the division';
    }

    get operationColor(){
        return '#7E7ACB';
    }

    action(a, b){
        return a % b;
    }

    getValidationErrors(a, b){
        let result = "";
        if(isNaN(a)){
            result +="First value should be defined\r\n";
        }

        if(isNaN(b)){
            result +="Second value should be defined\r\n";
        }

        if(b === 0){
            return "Second value should not be zero";
        }

        return result;
    }
}