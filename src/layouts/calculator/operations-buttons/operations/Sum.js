export default class Sum {
    get text(){
        return 'Summarize';
    }

    get operationColor(){
        return '#F59E8B';
    }

    action(a, b){
        return a + b;
    }

    getValidationErrors(a, b){
        let result = "";
        if(isNaN(a)){
            result +="First value should be defined\r\n";
        }

        if(isNaN(b)){
            result +="Second value should be defined\r\n";
        }

        return result;
    }
}