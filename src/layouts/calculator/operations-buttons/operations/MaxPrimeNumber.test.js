import React from "react";
import MaxPrimeNumber from "./MaxPrimeNumber";


describe("Max prime number operation", ()=> {

    describe("validation", ()=>{

        it("first and second values are valid should not return errors", () => {
            let maxPrimeNumber = new MaxPrimeNumber();

            let validationResult = maxPrimeNumber.getValidationErrors(5, 8);

            expect(validationResult).toBe("");
        });

        it("first value 'isNaN' should return validation error", () => {
            let maxPrimeNumber = new MaxPrimeNumber();

            let validationResult = maxPrimeNumber.getValidationErrors(parseInt(""), 8);

            expect(validationResult).toBe("First value should be defined\r\n");
        });

        it("second value 'isNaN' should return validation error", () => {
            let maxPrimeNumber = new MaxPrimeNumber();

            let validationResult = maxPrimeNumber.getValidationErrors(88, parseInt(""));

            expect(validationResult).toBe("Second value should be defined\r\n");
        });

        it("first and second values are 'isNaN' should return validation error", () => {
            let maxPrimeNumber = new MaxPrimeNumber();

            let validationResult = maxPrimeNumber.getValidationErrors(parseInt(""), parseInt(""));

            expect(validationResult).toBe("First value should be defined\r\nSecond value should be defined\r\n");
        });

        it("first value is less than second value should return validation error", () => {
            let maxPrimeNumber = new MaxPrimeNumber();

            let validationResult = maxPrimeNumber.getValidationErrors(298, 58);

            expect(validationResult).toBe("Second value should be greater than first value");
        });

        it("first value and second value are less than 2 should return validation error", () => {
            let maxPrimeNumber = new MaxPrimeNumber();

            let validationResult = maxPrimeNumber.getValidationErrors(-9, 0);

            expect(validationResult).toBe("Minimum prime number is 2");
        });
    })

    describe("color", ()=>{
        it("should be defined", () => {
            let maxPrimeNumber = new MaxPrimeNumber();

            expect(maxPrimeNumber.operationColor).toBe("#7ACB99");
        });
    })

    describe("text", ()=>{
        it("should be defined", () => {
            let maxPrimeNumber = new MaxPrimeNumber();

            expect(maxPrimeNumber.text).toBe("Max prime number from range");
        });
    })

    describe("action", ()=>{
        it("returns correct result", () => {
            let maxPrimeNumber = new MaxPrimeNumber();

            const result = maxPrimeNumber.action(8, 12);

            expect(result).toBe(11);
        });
    })
})
