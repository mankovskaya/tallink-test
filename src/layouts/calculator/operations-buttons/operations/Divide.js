export default class Divide {
    get text(){
        return 'Divide';
    }

    get operationColor(){
        return '#34B7BA';
    }

    action(a, b){
        return (a / b);
    }

    getValidationErrors(a, b){
        let result = "";
        if(isNaN(a)){
            result +="First value should be defined\r\n";
        }

        if(isNaN(b)){
            result +="Second value should be defined\r\n";
        }

        if(b === 0){
            return "Second value should not be zero";
        }

        return result;
    }
}