import React from "react";
import Divide from "./Divide";


describe("Divide operation", ()=> {

    describe("validation", ()=>{

        it("first and second values are valid should not return errors", () => {
            let division = new Divide();

            let validationResult = division.getValidationErrors(5, 8);

            expect(validationResult).toBe("");
        });

        it("first value 'isNaN' should return validation error", () => {
            let division = new Divide();

            let validationResult = division.getValidationErrors(parseInt(""), 2);

            expect(validationResult).toBe("First value should be defined\r\n");
        });

        it("second value 'isNaN' should return validation error", () => {
            let division = new Divide();

            let validationResult = division.getValidationErrors(9, parseInt(""));

            expect(validationResult).toBe("Second value should be defined\r\n");
        });

        it("first and second values are 'isNaN' should return validation error", () => {
            let division = new Divide();

            let validationResult = division.getValidationErrors(parseInt(""), parseInt(""));

            expect(validationResult).toBe("First value should be defined\r\nSecond value should be defined\r\n");
        });

        it("second value is 0 should return validation error", () => {
            let division = new Divide();

            let validationResult = division.getValidationErrors(7, 0);

            expect(validationResult).toBe("Second value should not be zero");
        });
    })

    describe("color", ()=>{
        it("should be defined", () => {
            let division = new Divide();

            expect(division.operationColor).toBe("#34B7BA");
        });
    })

    describe("text", ()=>{
        it("should be defined", () => {
            let division = new Divide();

            expect(division.text).toBe("Divide");
        });
    })

    describe("action", ()=>{
        it("returns correct result", () => {
            let division = new Divide();

            const result = division.action(10, 2);

            expect(result).toBe(5);
        });
    })
})
