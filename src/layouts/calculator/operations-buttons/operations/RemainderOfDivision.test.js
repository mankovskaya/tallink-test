import React from "react";
import RemainderOfDivision from "./RemainderOfDivision";


describe("Remainder of the division operation", ()=> {

    describe("validation", ()=>{

        it("first and second values are valid should not return errors", () => {
            let remainder = new RemainderOfDivision();

            let validationResult = remainder.getValidationErrors(5, 8);

            expect(validationResult).toBe("");
        });

        it("first value 'isNaN' should return validation error", () => {
            let remainder = new RemainderOfDivision();

            let validationResult = remainder.getValidationErrors(parseInt(""), 77);

            expect(validationResult).toBe("First value should be defined\r\n");
        });

        it("second value 'isNaN' should return validation error", () => {
            let remainder = new RemainderOfDivision();

            let validationResult = remainder.getValidationErrors(73, parseInt(""));

            expect(validationResult).toBe("Second value should be defined\r\n");
        });

        it("first and second values are 'isNaN' should return validation error", () => {
            let remainder = new RemainderOfDivision();

            let validationResult = remainder.getValidationErrors(parseInt(""), parseInt(""));

            expect(validationResult).toBe("First value should be defined\r\nSecond value should be defined\r\n");
        });

        it("second value is 0 should return validation error", () => {
            let remainder = new RemainderOfDivision();

            let validationResult = remainder.getValidationErrors(13, 0);

            expect(validationResult).toBe("Second value should not be zero");
        });
    })

    describe("color", ()=>{
        it("should be defined", () => {
            let remainder = new RemainderOfDivision();

            expect(remainder.operationColor).toBe("#7E7ACB");
        });
    })

    describe("text", ()=>{
        it("should be defined", () => {
            let remainder = new RemainderOfDivision();

            expect(remainder.text).toBe("Remainder of the division");
        });
    })

    describe("action", ()=>{
        it("returns correct result", () => {
            let remainder = new RemainderOfDivision();

            const result = remainder.action(70, 5);

            expect(result).toBe(0);
        });
    })
})
