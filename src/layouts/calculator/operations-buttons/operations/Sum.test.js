import React from "react";
import Sum from "./Sum";


describe("Sum operation", ()=> {

    describe("validation", ()=>{

        it("first and second values are valid should not return errors", () => {
            let sum = new Sum();

            let validationResult = sum.getValidationErrors(1, 1);

            expect(validationResult).toBe("");
        });

        it("first value 'isNaN' should return validation error", () => {
            let sum = new Sum();

            let validationResult = sum.getValidationErrors(parseInt(""), 2);

            expect(validationResult).toBe("First value should be defined\r\n");
        });

        it("second value 'isNaN' should return validation error", () => {
            let sum = new Sum();

            let validationResult = sum.getValidationErrors(2, parseInt(""));

            expect(validationResult).toBe("Second value should be defined\r\n");
        });

        it("first and second values are 'isNaN' should return validation error", () => {
            let sum = new Sum();

            let validationResult = sum.getValidationErrors(parseInt(""), parseInt(""));

            expect(validationResult).toBe("First value should be defined\r\nSecond value should be defined\r\n");
        });
    })

    describe("color", ()=>{
        it("should be defined", () => {
            let sum = new Sum();

            expect(sum.operationColor).toBe("#F59E8B");
        });
    })

    describe("text", ()=>{
        it("should be defined", () => {
            let sum = new Sum();

            expect(sum.text).toBe("Summarize");
        });
    })

    describe("action", ()=>{
        it("returns correct result", () => {
            let sum = new Sum();

            const result = sum.action(5, 9);

            expect(result).toBe(14);
        });
    })
})
