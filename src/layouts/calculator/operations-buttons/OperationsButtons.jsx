import React, { useContext, setState } from 'react';
import Context from "../../../context";
import classes from './OperationsButtons.module.scss';

const OperationsButtons = ({ firstField, secondField }) => {
    const { availableOperations, resultsHistory, updateHistory } = useContext(Context);

    let calculate = (operation, event) => {
        event.preventDefault();
        let validationErrors = operation.getValidationErrors(parseInt(firstField), parseInt(secondField));

        if(validationErrors){
            alert(validationErrors);
            return;
        }
        const result = operation.action(firstField, secondField);
        updateHistory(
            resultsHistory,
            firstField,
            secondField,
            operation.operationColor,
            isNaN(result) ? result : result.toFixed(2))
    }

    return (
        <div data-testid = "buttons-container" className={classes.buttonsWrapper}>
            {availableOperations.map((operation, index) =>
                <button key={index}
                        type='submit'
                        data-testid = "operation-button"
                        className={classes.operationButton}
                        style={{backgroundColor: operation.operationColor}}
                        onClick={(event) => {calculate(operation, event)}}>
                    {operation.text}
                </button>)}
        </div>
    );
}

export default OperationsButtons;
