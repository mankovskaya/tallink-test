import React, {useContext} from 'react';
import OperationsButtons from "./operations-buttons/OperationsButtons";
import Context from '../../context';
import classes from './Calculator.module.scss';

const Calculator = ({firstField, secondField}) => {
    const {setFirstField, setSecondField} = useContext(Context);
    return (
        <div data-testid="fields-container" className={classes.formWrapper}>
            <div className={classes.fieldsWrapper}>
                <div className={classes.fieldBlock}>
                    <label>
                        First value:
                    </label>
                    <input
                        data-testid="first-field"
                        type="number"
                        value={firstField}
                        onChange={event => setFirstField(parseInt(event.target.value))}/>
                </div>
                <div className={classes.fieldBlock}>
                    <label>
                        Second value:
                    </label>
                    <input
                        data-testid="second-field"
                        type="number"
                        value={secondField}
                        onChange={event => setSecondField(parseInt(event.target.value))}/>
                </div>
            </div>
            <OperationsButtons
                className={classes.buttonsWrapper}
                firstField={firstField}
                secondField={secondField}/>
        </div>
    );
}

export default Calculator;
